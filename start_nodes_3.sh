#!/bin/bash

NODEA=127.0.0.1:2010
NODEB=127.0.0.1:2020
NODEC=127.0.0.1:2030


case $1 in
    "A")
        python Node.py NodeA $NODEA $NODEB $NODEC --console
        ;;
    "B")
        python Node.py NodeB $NODEB $NODEA --console
        ;;
    "C")
        python Node.py NodeC $NODEC $NODEA --console
        ;;
    *)
        python Node.py NodeC $NODEC $NODEA &
        sleep 0.1
        python Node.py NodeB $NODEB $NODEA &
        sleep 0.1
        python Node.py NodeA $NODEA $NODEB $NODEC --console
        ;;
esac
