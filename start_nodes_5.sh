#!/bin/bash

NODEA=192.168.122.144:2010
NODEB=192.168.122.174:2020
NODEC=192.168.122.119:2030
NODED=192.168.122.40:2040
NODEE=192.168.122.57:2050


case $1 in
    "A")
        python Node.py NodeA $NODEA $NODEB $NODED --console
        ;;
    "B")
        python Node.py NodeB $NODEB $NODEA $NODEC --console
        ;;
    "C")
        python Node.py NodeC $NODEC $NODEB $NODED --console
        ;;
    "D")
        python Node.py NodeD $NODED $NODEA $NODEC $NODEE --console
        ;;
    "E")
        python Node.py NodeE $NODEE $NODED --console
        ;;
    *)
        python Node.py NodeE $NODEE $NODED &
        sleep 0.1
        python Node.py NodeD $NODED $NODEA $NODEC $NODEE &
        sleep 0.1
        python Node.py NodeC $NODEC $NODEB $NODED &
        sleep 0.1
        python Node.py NodeB $NODEB $NODEA $NODEC &
        sleep 0.1
        python Node.py NodeA $NODEA $NODEB $NODED --console
        ;;
esac
