
import numpy as np
from hashlib import sha256

ALPHABET = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
PASS_LENGTH = 5


def base26_to_base10(n: list[int]) -> int:
    res = 0
    for index, digit in enumerate(reversed(n)):
        res += digit * 26 ** index
    return res

def base10_to_base26(n: int) -> list[int]:
    res = []
    while n != 0:
        remainder = n % 26
        n //= 26
        res.append(remainder)

    while len(res) < PASS_LENGTH:
        res.append(0)
    return list(reversed(res))

def string_to_list(s: str) -> list[int]:
    res = []
    for char in s:
        try:
            res.append(ALPHABET.index(char))
        except ValueError as e:
            print("Bad char in string:" + char)
            raise
    return res

def list_to_string(l: list[int]) -> str:
    res = ""
    for n in l:
        try:
            res += ALPHABET[n]
        except Exception as e:
            raise
    return res

def string_to_number(s: str) -> int:
    return base26_to_base10(string_to_list(s))

def number_to_string(n: int) -> str:
    return list_to_string(base10_to_base26(n))

def hash_search(hhash: str, start: int, end: int) -> str | None:
    for i in range(start, end):
        string = number_to_string(i)
        if sha256(string.encode()).hexdigest() == hhash:
            return string
    return ""

def split_interval_pairs(start, end, n):
    intervals = np.linspace(start, end, num=n+1, endpoint=True, dtype=int)
    pairs = [(intervals[i], intervals[i+1]) for i in range(n)]
    return pairs

def upper_limit():
    return string_to_number("z" * PASS_LENGTH) + 1

def max_work_chunk_size():
    return upper_limit() // 20


if __name__ == "__main__":

    print(max_work_chunk_size())
    print("upper limit: " + str(upper_limit()))

    password = "hkljp"
    hhash = sha256(password.encode()).hexdigest()
    print("hash " + hhash)

    print("string to number: " + str(string_to_number(password)))


    intervals = split_interval_pairs(0, upper_limit(), 200)
    for interval in intervals:

        print(f"Searching from: {number_to_string(interval[0])} to: {number_to_string(interval[1] - 1)}")
        result = hash_search(hhash, interval[0], interval[1])

        if result == None:
            print("not found")
        else:
            print("password is: " + result)
            break
