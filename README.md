# DSV Semestrální práce

| | |
| ---- | ---- |
| **autor** | Patrik Novák |
| **typ problému** | Ukončení výpočtu |
| **algoritmus** | Dijkstra-Scholten |
| **jazyk** | Python |
| **transport zpráv** | gRPC |
| **funkcionalita** | Počítání hash |

## Popis

Úkolem systému je prolomit heslo hashované pomocí SHA-256. Pro jednoduchost jsou prohledávány pouze řetězce o délce 5 a obsahující pouze znaky malé anglické abecedy (26 znaků). Systém prolamuje hash pomocí brute-force tzn. zkouší postupně hashovat všechny řetězce délky 5 a výsledek porovnává se vstupní hash.

## Dělení práce

Celkový počet možností, které je třeba vyzkoušet je pevně daný: **11881376**. Limit pro dělení je také pevně daný: **1697339**.

Jakmile uzel dostane zprávu, kde je množství práce vyšší nežli limit pro dělení, "ukrojí" si z práce část pro sebe (o velikosti limitu pro dělení) a zbytek rozdělí rovnoměrně mezi své sousedy.

Toto dělení probíhá to té doby, kdy je všechna práce rozdělena.

Přijatou práci si uzel eviduje pomocí třídy `work_chunk`.
Tato třída má následující atributy:

```python
id: str  # id uzlu, od kterého přišel požadavek
start: int  # začátek intervalu, který je potřeba projít
end: int  # konec intervalu, který je potřeba projít
intervals: list[(int,int)]  # intervaly na které se práce rozpadla
result: str  # úložiště pro výsledek výpočtu
```

Po dokončení části výpočtu nebo přijetí signálu je vyhledá příslušný `work_chunk` a smaže se příslušný interval s proměnné `intervals`.
Na požadavek uzel odpoví až tehdy když jsou prohledány všechny intervaly (`intervals` je prázdný).

### Poskytnutí práce sám sobě

Uzel může nepřímo poskytnout práci sám sobě díky tomu, že při dělení práce posílá požadavky všem sousedům nehledě na to, zda je to parent či nikoliv.

## Typy zpráv

Uzel umí pracovat se 2 typy požadavků/zpráv.

### MESSAGE

Tato zpráva je požadavek na práci. Obsahuje `id` odesílatele, `hash`, kterou chceme prolomit, začátek a konec (`start`, `end`) intervalu výpočtu a informaci o tom zda je to počáteční požadavek (`initial`).

Po obdržení požadavku se uzel rozhodne, zda li je potřebe práci dělit a podle toho vytvoří příslušný `work_chunk` a začne část počítat, případně pošle část výpočtu dál.

### SIGNAL

Tato zpráva nese informaci o tom, že byla určitá část problému dopočítána. Obsahuje `id` odesílatele, začátek a konec (`start`, `end`) intervalu výpočtu a případný výsledek (`password`).

Po obdržení signálu se nejprve vyhledá příslušný `work_chunk` a příslušný interval označíme za vyřešený (smažeme ho). Pokud je `work_chunk` spočítán celý, pošle se SIGNAL příslušnému uzlu a `work_chunk` smažeme.

## Topologie

Topologie použitá pro testování.

![Topologie](/nodes.svg)


## Jak projekt zprovoznit

### Příprava prostředí
```bash
python -m venv venv
source venv/bin/activate
pip install --upgrade pip grpcio grpcio_tools numpy
./compile_grpc.sh
```

### Samotné spuštění

Příklad: Chci spustit uzel se jménem `UzelA` (využito pro logování) na IP `192.168.122.3` a portu `2010` a nastavit uzly `192.168.122.4:2020` a `192.168.122.5:2030` jako jeho sousedy. Také chci aby se spustila konzole.

```bash
python Node.py UzelA 192.168.122.3:2010 192.168.122.4:2020 192.168.122.5:2030 --console
```

### Spuštení algoritmu

Zadáním příkazu `crack` do konzole se spustí výpočet. Vstupní hash je pro jednoduchost pevně daná.
