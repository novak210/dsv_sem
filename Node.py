import argparse
from concurrent import futures
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass, field
import logging
from time import sleep

import grpc
import passcrack_pb2
import passcrack_pb2_grpc

import MyHash


@dataclass
class WorkChunk:
    id: str
    start: int
    end: int
    intervals: list = field(default_factory=list)
    result: str = field(default="", compare=False)

    def is_done(self) -> bool:
        return len(self.intervals) == 0
    
    def contains_interval(self, start: int, end: int) -> bool:
        for interval in self.intervals:
            if interval[0] == start and interval[1] == end:
                return True
        return False
        
    def signal(self, interval: int, result: str):
        self.intervals.remove(interval)
        if result != "" and self.result == "":
            self.result = result


@dataclass
class Node(passcrack_pb2_grpc.PassCracker):
    name: str
    
    logger: logging.Logger = field(init=False)
    console: bool = False
    delay: float = 0.3  # delay in s

    id: str = "127.0.0.1:2010"
    neighbours: list = field(default_factory=list)

    defin: int = 0
    defout: int = 0

    # id of node we've got first request from
    parent: str = None

    # id's of nodes we've got request
    others: list = field(default_factory=list)

    initiator: bool = False

    # executor for computing hashes in background
    executor: ThreadPoolExecutor = field(init=False)

    work_chunks: list = field(default_factory=list)

    def reset(self):
        self.defin = 0
        self.defout = 0
        self.parent = None
        self.others = []
        self.initiator = False
        self.work_chunks = []

    def __post_init__(self):
        self.executor = ThreadPoolExecutor(max_workers=30)

        # Logger settings
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(logging.DEBUG)

        # Create a file handler
        fh = logging.FileHandler(f"{self.name}.log")
        fh.setLevel(logging.DEBUG)

        # Create a console handler
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # Create a formatter
        formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)7s <> %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # Add handlers
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)


    def RecieveMessage(self, request: passcrack_pb2.Message, context) -> passcrack_pb2.GenericReply:
        """
        Recieve a MESSAGE.
        """
        self.logger.info(f"Got MESSAGE from {request.senderid}, start={request.start}, end={request.end}")
        if request.initial:
            self.logger.debug(f"INITIAL {self.initiator} -> True")        
            self.initiator = True
        
        if self.defin == 0:
            self.parent = request.senderid
        else:
            self.others.append(request.senderid)

        self.logger.debug(f"DEFIN {self.defin} -> {self.defin + 1}")
        self.defin += 1

        start = request.start
        end = request.end
        total_work = end - start

        # create a work chunk
        self.logger.debug(f"Creating a work chunk: id={request.senderid}, start={request.start}, end={request.end}")
        work_chunk = WorkChunk(request.senderid, request.start, request.end)

        # split?
        if total_work > MyHash.max_work_chunk_size() and len(self.neighbours) > 0:
            end = start + MyHash.max_work_chunk_size()
            work_chunk.intervals.append((start, end))
            self.work_chunks.append(work_chunk)
            # do the computation

            self.executor.submit(self.RunPassCracker, request.hash, start, end)

            intervals = MyHash.split_interval_pairs(end, request.end, len(self.neighbours))
            self.logger.debug(f"Splitting remaining work to chunks: {intervals}")

            # send remaining to others
            for neighbour, interval in zip(self.neighbours, intervals):
                work_chunk.intervals.append(interval)
                response = self.SendMessage(neighbour, request.hash, interval)
                # self.logger.debug(f"Got PassCrackReply from {neighbour}, message={response}")
        else:
            work_chunk.intervals.append((start, end))
            self.work_chunks.append(work_chunk)
            self.executor.submit(self.RunPassCracker, request.hash, start, end)
        return passcrack_pb2.GenericReply(message="OK")
    
    def RecieveSignal(self, request: passcrack_pb2.Signal, context) -> passcrack_pb2.GenericReply:
        """
        Recieve a SIGNAL.
        """
        self.logger.info(f"Got SIGNAL from {request.senderid}: start={request.start}, end={request.end}, password={request.password}")
        work_chunk = self.get_work_chunk(request.start, request.end)
        if work_chunk == None:
            self.logger.warning(f"Work chunk does not exist {request.start}, {request.end}")
            return passcrack_pb2.GenericReply(senderid=self.id, message="Thanks!")

        work_chunk.signal((request.start, request.end), request.password)

        self.logger.debug(f"DEFOUT {self.defout} -> {self.defout - 1}")
        self.defout -= 1

        if self.defin == 1 and self.defout == 0:
            # reseting node
            self.defin = 0
            self.parent = None
            self.work_chunks = []

            if self.initiator:
                self.logger.info(f"Computation terminated! Password is '{work_chunk.result}'")
                self.initiator = False 
            else:
                self.SendSignal(work_chunk.id, work_chunk.start, work_chunk.end, work_chunk.result)
            return passcrack_pb2.GenericReply(senderid=self.id, message="Node work done.")
        
        if self.defin > 1 and work_chunk.is_done():
            self.logger.debug(f"DEFIN {self.defin} -> {self.defin - 1}")
            self.defin -= 1
            try:
                self.others.remove(request.senderid)  # remove sender who sent this signal from others
            except ValueError:
                self.logger.warning(f"Cannot remove: {work_chunk.id} not in {self.others}")
            try:
                self.work_chunks.remove(work_chunk)
            except ValueError:
                self.logger.warning(f"Cannot remove: {work_chunk} not in {self.work_chunks}")
            self.SendSignal(work_chunk.id, work_chunk.start, work_chunk.end, work_chunk.result)

        return passcrack_pb2.GenericReply(senderid=self.id, message="Thanks!")
    
    def SendMessage(self, other_node, target_hash, interval, initial=False) -> passcrack_pb2.GenericReply:
        """
        Send a MESSAGE.
        """
        start, end = interval
        if not initial:
            self.logger.info(f"Send MESSAGE to {other_node}, start={start}, end={end}")
            self.logger.debug(f"DEFOUT {self.defout} -> {self.defout + 1}")
            self.defout += 1
        with grpc.insecure_channel(other_node) as channel:
            stub = passcrack_pb2_grpc.PassCrackerStub(channel)
            sleep(self.delay)
            response = stub.RecieveMessage(passcrack_pb2.Message(senderid=self.id, hash=target_hash, start=start, end=end, initial=initial))
            # self.logger.debug(f"Got reply from {other_node}, message={response}")
            return response
        
    def SendSignal(self, other_node, start, end, password):
        """
        Send a SIGNAL.
        """
        self.logger.info(f"Send SIGNAL to {other_node}, start={start}, end={end}, password={password}")
        with grpc.insecure_channel(other_node) as channel:
            stub = passcrack_pb2_grpc.PassCrackerStub(channel)
            sleep(self.delay)
            response = stub.RecieveSignal(passcrack_pb2.Signal(senderid=self.id, start=start, end=end, password=password))
            # self.logger.debug(f"Got SIGNAL reply from {other_node}, message={response.message}")
            return response
        
    def RunPassCracker(self, target_hash, start, end):
        """
        Do the COMPUTATION.
        """
        self.logger.info(f"Cracking the password: hash={target_hash}, start={start}, end={end}")
        
        work_chunk = self.get_work_chunk(start, end)
        if work_chunk == None:
            self.logger.warning(f"Work chunk does not exist {start}, {end}")
            return

        tmpres = ""
        if work_chunk.result == "":
            # result = "peekaboo"
            result = MyHash.hash_search(target_hash, start, end)
            # hash computation takes a long time, se there is a possibility that `result`
            # is changed when the computation ends
            if result != "":
                tmpres = result

        # self.logger.debug(f"Cracking the password. DEFIN={self.defin}, DEFOUT={self.defout}, {work_chunk}")
        work_chunk.signal((start, end), tmpres)
        
        if self.defin == 1 and self.defout == 0:
            # reseting node
            self.defin = 0
            self.parent = None
            self.work_chunks = []

            if self.initiator:
                self.logger.info(f"Computation terminated! Password is '{result}'")
                self.initiator = False
            else:
                self.SendSignal(work_chunk.id, work_chunk.start, work_chunk.end, work_chunk.result)
        
        if self.defin > 1 and work_chunk.is_done():
            self.logger.debug(f"DEFIN {self.defin} -> {self.defin - 1}")
            self.defin -= 1
            try:
                self.work_chunks.remove(work_chunk)
            except ValueError:
                self.logger.warning(f"Cannot remove: {work_chunk} not in {self.work_chunks}")
            self.SendSignal(work_chunk.id, work_chunk.start, work_chunk.end, work_chunk.result)

    def get_work_chunk(self, start: int, end: int) -> WorkChunk | None:
        for work_chunk in self.work_chunks:
            if work_chunk.contains_interval(start, end):
                return work_chunk
        return None

    def start_console_handler(self):
        # self.logger.debug("Console handler started.")
        while True:
            command = input("Type a command: ")

            if command == "exit":
                break
            if command == "?":
                print("? - this help")
                print("s - print node status")
                print("r - reset node state")
                print("d <float> - set delay in seconds")
                print("passcrack - try to crack a password")
                print("exit - stop node")
                print()
            elif command == "s":
                print(self)
            elif command == "r":
                self.reset()
            elif command.startswith("d"):
                _, f = command.split(" ")
                try:
                    self.delay = float(f)
                except:
                    print("Bad format")

            elif command == "crack":
                # target_hash = input("Insert hash: ")
                print("Starting cracking...")
                from hashlib import sha256
                self.SendMessage(self.id, sha256("hello".encode()).hexdigest(), (0, MyHash.upper_limit()), initial=True)
            else:
                print("Unrecognized command.")

    
    def run(self):
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=30))
        passcrack_pb2_grpc.add_PassCrackerServicer_to_server(self, server)
        server.add_insecure_port(self.id)

        try:
            server.start()
            self.logger.info(f"Server {self.name} started on {self.id}")
            if self.console:
                self.start_console_handler()
            else:
                server.wait_for_termination()
        except KeyboardInterrupt:
            print("Recieved Ctrl-C")
        finally:
            self.logger.info("Stopping server...")
            server.stop(1)
            self.logger.info("Stopping executor...")
            self.executor.shutdown()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="DSV Node",
        description="Node does provide passcracking utility",
    )
    parser.add_argument('name',
                        help="Name of the node")
    parser.add_argument('ipport',
                        help="IP address and port on which the node will be available.")
    parser.add_argument('neighbours',
                        nargs='*',
                        help="Addresses and ports of neighbours.")
    parser.add_argument('--console',
                        action='store_true')
    
    args = parser.parse_args()

    node = Node(name=args.name, id=args.ipport, neighbours=args.neighbours, console=args.console)
    node.run()
